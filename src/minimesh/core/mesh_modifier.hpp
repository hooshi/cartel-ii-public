#pragma once

//
// mesh_modifier.hpp
//
// Some functionality for modifying meshes.
//
// Author: Shayan Hoshyari
//


#include <string>

#include <minimesh/core/mesh_connectivity.hpp>

namespace minimesh
{

class Mesh_modifier
{
public:
  // Trivial constructor
  Mesh_modifier(Mesh_connectivity & mesh_in): _m(mesh_in) {}

  // Get the underlying mesh
  Mesh_connectivity & mesh() { return _m; }
  const Mesh_connectivity & mesh() const { return _m; }

  //
  // Given two vertices, this function return the index of the half-edge going from v0 to v1.
  // Returns mesh::invalid_index if no half-edge exists between the two vertices.
  //
  int get_halfedge_between_vertices(const int v0, const int v1);

  //
  // Flip an edge in a mesh
  // Input: The mesh, and the index of a half-edge on the edge we wish to flip
  // Return true if operation successful, and false if operation not possible
  //
  // Assumption: mesh is all triangles
  //
  // NOTE: To see how this method works, take a look at edge-flip.svg
  //
  bool flip_edge(const int he_index);

  // 
  // Collapse an the edge that contains the given half-edge.
  //
  // Returns true if operation passes some elementary tests.
  // However, note that the checks are not comprehensive and may still break
  // the manifoldness of the mesh. It is up to you to complement the tests
  // such that manifoldness is guaranteed.
  //
  // NOTE: After collpase he.dest() vertex will be removed from the mesh
  //       But he.origin() will remain in the mesh, and retain its index and position
  //       You can simply change the position either before or after calling this function
  //       if you want.
  //
  // NOTE: This code is midly tested. But it might have bugs.
  //
  // NOTE: To see how this method works, take a look at edge-collapse.svg
  // 
  bool collapse_edge(const int he_id);


private:
  // pointer to the mesh that we are working on.
  Mesh_connectivity & _m;
};


} // end of minimesh
